'use strict'

const Opal = global.Opal
const PreprocessorReader = Opal.module(null, 'Asciidoctor').PreprocessorReader
const Cursor = Opal.module(null, 'Asciidoctor').Reader.Cursor
var $hash2 = Opal.hash2

module.exports.register = function (registry, config = {}) {
  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  // Perhaps this could be useful? probably not.
  // const vfs = (config.vfs && typeof config.vfs.read === 'function')
  //   ? config.vfs
  //   //Antora support
  //   : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
  //     ? {
  //       read: (resourceId) => {
  //         const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
  //         return target.contents
  //       },
  //     }
  //     //look in the file system
  //     : fsAccess()
  //
  // function fsAccess () {
  //   const fs = require('fs')
  //   return {
  //     read: (path, encoding = 'utf8') => {
  //       if (path.startsWith('file://')) {
  //         return fs.readFileSync(path.substr('file://'.length), encoding)
  //       }
  //       return fs.readFileSync(path, encoding)
  //     },
  //   }
  // }

  function exampleBlockMacro () {
    const self = this
    self.named('example')
    self.process(function (parent, target, attrs) {
      var output = `
[source, role="example"]
----
include::example$example/Function-variant.js[]
----
`.split('\n')
      const document = parent.document
      const reader = PreprocessorReader.$new(
        document,
        output,
        Cursor.$new(
          document.attributes['$[]']('docfile'),
          document.base_dir
        ),
        $hash2(
          ['normalize'],
          { normalize: true }
        )
      )
      const wrapper = self.createBlock(parent, 'open', [], {})
      // wrapper.reader = reader
      self.parseContent(wrapper, reader)
      parent.blocks.push(wrapper)
    })
  }

  function doRegister (registry) {
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(exampleBlockMacro)
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
