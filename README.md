# Asciidoctor example Extension
:version: 0.0.1

`@me/asciidoctor-example` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/me/asciidoctor-example/-/blob/master/README.adoc.

## Installation

Available soon through npm as @me/asciidoctor-example.

Currently available through a line like this in `package.json`:


    "@me/asciidoctor-example": "https://experimental-repo.s3-us-west-1.amazonaws.com/me-asciidoctor-example-v0.0.1.tgz",


The project git repository is https://gitlab.com/me/asciidoctor-example

## Usage in asciidoctor.js

see https://gitlab.com/me/asciidoctor-example/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/me/asciidoctor-example/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/example-extension in `https://gitlab.com/me/simple-examples`.
