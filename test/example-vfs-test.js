/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const example = require('./../lib/example')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

function registerIncludeProcessor (registry, config = {}) {
  function testIncludeProcessor () {
    const self = this
    self.handles(function (target) {
      return true//decide if this include processor applies to this target.
    })
    self.process(function (doc, reader, target, attributes) {
      var data = [`Including ${target}`]//array of (String) lines
      return reader.pushInclude(data, target, target, 1, attributes)// don't understand lineNo?
    })
  }

  function doRegister (registry) {
    if (typeof registry.includeProcessor === 'function') {
      registry.includeProcessor(testIncludeProcessor)
    } else {
      console.warn('no \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}

describe('example tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[{
    type: 'global',
    f: (text, config) => {
      example.register(asciidoctor.Extensions, config)
      registerIncludeProcessor(asciidoctor.Extensions, config)
      return asciidoctor.load(text, {
        attributes: { 'allow-uri-read': 'true' },
        safe: 'safe',
      })
    },
  },
  {
    type: 'registry',
    f: (text, config) => {
      const registry = example.register(asciidoctor.Extensions.create(), config)
      registerIncludeProcessor(registry, config)
      return asciidoctor.load(text, {
        extension_registry: registry,
        attributes: { 'allow-uri-read': 'true' },
        safe: 'safe',
      })
    },
  }].forEach(({ type, f }) => {
    prepareVfss({
      version: '4.5',
      family: 'page',
      relative: 'page-a.adoc',
      contents: `== test sub page

`,
    }).forEach(({ vfsType, config }) => {
      it(`something about the test, ${type}, ${vfsType}`, () => {
        const doc = f(`= test page
        
include::page-a.adoc[]

example::foo[bar]

`, config)
        const html = doc.convert()
        expect(html).to.equal(`<div class="paragraph">
<p>Including page-a.adoc</p>
</div>
<div class="openblock">
<div class="content">
<div class="listingblock example">
<div class="content">
<pre class="highlight"><code>Including example$example/Function-variant.js</code></pre>
</div>
</div>
</div>
</div>`)
      })
    })
  })
})
